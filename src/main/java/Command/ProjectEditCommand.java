package Command;

import Entity.Project;
import Repository.ProjectRepository;

import java.util.Scanner;
import java.util.UUID;

public class ProjectEditCommand extends AbstractCommand {
    private final String name = "project-edit";
    private final String description = "Edit project.";
    private final ProjectRepository projectRepository;
    private final Scanner scanner;

    public ProjectEditCommand(ProjectRepository projectRepository, Scanner scanner) {
        this.projectRepository = projectRepository;
        this.scanner = scanner;
    }

    @Override
    public void handle() {

        System.out.println("Введите uuid проекта:");
        String uuid = scanner.nextLine();

        System.out.println("Введите новое название проекта:");
        String name = scanner.nextLine();

        projectRepository.update(uuid, name);

        System.out.println("Проект обновлён.");
    }
}
