package Command;


public abstract class AbstractCommand {
    protected String name;
    protected String description;

    public abstract void handle();

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name + ": " + description;
    }
}
