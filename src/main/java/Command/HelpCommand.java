package Command;

import java.util.ArrayList;
import java.util.Scanner;

public class HelpCommand extends AbstractCommand {
    private final String name = "help";
    private final String description = "Show all commands.";
    private ArrayList<AbstractCommand> commands;

    public HelpCommand(ArrayList<AbstractCommand> commands) {
        this.commands = commands;
    }

    @Override
    public void handle() {
        StringBuilder message = new StringBuilder();

        for (AbstractCommand command: commands) {
            message.append(command.toString())
                    .append("\n");
        }

        System.out.println(message);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
