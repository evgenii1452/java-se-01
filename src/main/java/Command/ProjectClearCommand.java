package Command;

import Repository.ProjectRepository;

import java.util.Scanner;

public class ProjectClearCommand extends AbstractCommand {
    private final String name = "project-clear";
    private final String description = "Remove all project.";
    private final ProjectRepository projectRepository;

    public ProjectClearCommand(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }
    @Override
    public void handle() {
        projectRepository.deleteAll();

        System.out.println("Все проекты удалены.");
    }
}
