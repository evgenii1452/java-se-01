package Command;

import Repository.TaskRepository;

import java.util.Scanner;

public class TaskRemoveCommand extends AbstractCommand {
    private final String name = "task-remove";
    private final String description = "Remove task.";
    private final TaskRepository taskRepository;
    private final Scanner scanner;

    public TaskRemoveCommand(TaskRepository taskRepository, Scanner scanner) {
        this.taskRepository = taskRepository;
        this.scanner = scanner;
    }

    @Override
    public void handle() {
        System.out.println("Введите id задачи:");
        String uuid = scanner.nextLine();

        taskRepository.delete(uuid);

        System.out.println("Задача удалена.");
    }
}
