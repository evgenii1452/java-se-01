package Command;

import Repository.ProjectRepository;
import Repository.TaskRepository;

import java.util.Scanner;

public class TaskEditCommand extends AbstractCommand {
    private final String name = "task-edit";
    private final String description = "Edit task.";
    private final TaskRepository taskRepository;
    private final Scanner scanner;

    public TaskEditCommand(TaskRepository taskRepository, Scanner scanner) {
        this.taskRepository = taskRepository;
        this.scanner = scanner;
    }

    @Override
    public void handle() {

        System.out.println("Введите uuid задачи:");
        String uuid = scanner.nextLine();

        System.out.println("Введите новое название задачи:");
        String name = scanner.nextLine();

        if (taskRepository.update(uuid, name)) {
            System.out.println("Задача обновлена.");

            return;
        }

        System.out.println("Задача не найдена");
    }
}
