package Command;

import Repository.ProjectRepository;

import java.util.Scanner;

public class ProjectRemoveCommand extends AbstractCommand {
    private final String name = "project-remove";
    private final String description = "Remove project.";
    private final ProjectRepository projectRepository;
    private Scanner scanner;

    public ProjectRemoveCommand(ProjectRepository projectRepository, Scanner scanner) {
        this.projectRepository = projectRepository;
        this.scanner = scanner;
    }
    @Override
    public void handle() {
        System.out.println("Введите id проекта");
        String uuid = scanner.nextLine();

        projectRepository.delete(uuid);

        System.out.println("Проект удален");
    }
}
