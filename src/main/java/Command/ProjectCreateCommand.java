package Command;

import Entity.Project;
import Repository.ProjectRepository;

import java.util.Scanner;
import java.util.UUID;

public class ProjectCreateCommand extends AbstractCommand {
    private final String name = "project-create";
    private final String description = "Create new project.";
    private final ProjectRepository projectRepository;
    private Scanner scanner;

    public ProjectCreateCommand(ProjectRepository projectRepository, Scanner scanner) {
        this.projectRepository = projectRepository;
        this.scanner = scanner;
    }

    @Override
    public void handle() {
        System.out.println("Введите название проекта:");

        String name = scanner.nextLine();

        Project project = new Project(UUID.randomUUID().toString(), name);
        projectRepository.persist(project);

        System.out.println("Проект создан.");
    }
}
