package Command;

import Entity.Project;
import Entity.Task;
import Repository.ProjectRepository;
import Repository.TaskRepository;

import java.util.Scanner;

public class TaskListCommand extends AbstractCommand {
    private final String name = "task-list";
    private final String description = "Show all tasks.";
    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;

    public TaskListCommand(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void handle(Scanner scanner) {
        StringBuilder stringBuilder = new StringBuilder();

        for (Project project: projectRepository.getAll()) {
            stringBuilder.append(project.getUuid())
                    .append(". ")
                    .append(project.getName())
                    .append("\n");
        }

        System.out.println(stringBuilder);

        System.out.println("Введите uuid проекта:");
        String projectUuid = scanner.nextLine();
        Project project = projectRepository.find(projectUuid);

        for (Task task: taskRepository.getAll(project.getUuid())) {
            System.out.println(task.getUuid() + ": " + task.getName());
        }

    }
}
