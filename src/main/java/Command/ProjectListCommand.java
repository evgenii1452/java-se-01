package Command;

import Entity.Project;
import Repository.ProjectRepository;

import java.util.Scanner;

public class ProjectListCommand extends AbstractCommand {
    private final String name = "project-list";
    private final String description = "Show all projects.";
    private final ProjectRepository projectRepository;

    public ProjectListCommand(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void handle() {
        StringBuilder stringBuilder = new StringBuilder();

        for (Project project: projectRepository.getAll()) {
            if (project == null) {
                continue;
            }

            stringBuilder.append(project.getUuid())
                    .append(". ")
                    .append(project.getName())
                    .append("\n");
        }

        System.out.println(stringBuilder);
    }
}
