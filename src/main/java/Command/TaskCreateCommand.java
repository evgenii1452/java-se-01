package Command;

import Entity.Project;
import Entity.Task;
import Repository.ProjectRepository;
import Repository.TaskRepository;

import java.util.Scanner;
import java.util.UUID;

public class TaskCreateCommand extends AbstractCommand {
    private final String name = "task-create";
    private final String description = "Create new task.";
    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;
    private final Scanner scanner;

    public TaskCreateCommand(
            ProjectRepository projectRepository,
            TaskRepository taskRepository,
            Scanner scanner
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.scanner = scanner;
    }

    @Override
    public void handle() {
        System.out.println("Введите uuid проекта:");
        String projectUuid = scanner.nextLine();

        Project project = projectRepository.find(projectUuid);

        if (project == null) {
            System.out.println("Проект не найден.");

            return;
        }

        System.out.println("Введите название задачи:");
        String taskName = scanner.nextLine();

        Task task = new Task(project.getUuid(), UUID.randomUUID().toString(), taskName);
        taskRepository.persist(task);

        System.out.println("Задача создана.");
    }
}
