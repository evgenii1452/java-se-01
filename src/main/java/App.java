import Bootstrap.Bootstrap;
import Command.AbstractCommand;
import Repository.ProjectRepository;
import Repository.TaskRepository;

import java.util.ArrayList;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        ProjectRepository projectRepository = new ProjectRepository();
        TaskRepository taskRepository = new TaskRepository();
        Scanner input = new Scanner(System.in);
        ArrayList<AbstractCommand> commands = Bootstrap.initCommands(projectRepository, taskRepository, input);

        System.out.println("*** WELCOME TO TASK MANAGER ***");


        while (true) {
            String commandName = input.nextLine();

            for (AbstractCommand command : commands) {
                if (commandName.equals(command.getName())) {
                    command.handle();

                    break;
                }
            }

            if (commandName.equals("exit")) {
                break;
            }
        }

        System.out.println("*** GOODBYE ***");
    }
}
