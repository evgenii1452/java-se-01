package Bootstrap;

import Command.*;
import Repository.ProjectRepository;
import Repository.TaskRepository;

import java.util.ArrayList;
import java.util.Scanner;

public class Bootstrap {

    public static ArrayList<AbstractCommand> initCommands(
            ProjectRepository projectRepository,
            TaskRepository taskRepository,
            Scanner scanner
    ) {
        ArrayList<AbstractCommand> commands = new ArrayList<>();

        commands.add(new ProjectListCommand(projectRepository));
        commands.add(new ProjectCreateCommand(projectRepository, scanner));
        commands.add(new ProjectEditCommand(projectRepository, scanner));
        commands.add(new ProjectRemoveCommand(projectRepository, scanner));
        commands.add(new ProjectClearCommand(projectRepository));
        commands.add(new TaskListCommand(projectRepository, taskRepository));
        commands.add(new TaskCreateCommand(projectRepository, taskRepository, scanner));
        commands.add(new TaskRemoveCommand(taskRepository, scanner));
        commands.add(new TaskEditCommand(taskRepository, scanner));
        commands.add(new HelpCommand(commands));

        return commands;
    }
}
