package Repository;

import Entity.Project;

import java.util.ArrayList;

public class ProjectRepository {
    ArrayList<Project> projects = new ArrayList<>();

    public void persist(Project project) {
        projects.add(project);
    }

    public void update(String uuid, String projectName) {
        for (Project project: projects) {
            if (project.getUuid().equals(uuid)) {
                project.setName(projectName);
            }
        }
    }

    public ArrayList<Project> getAll() {
        return projects;
    }

    public Project find(String uuid) {
        for (Project project: projects) {
            if (project.getUuid().equals(uuid)) {
                return project;
            }
        }

        return null;
    }

    public void delete(String uuid) {
        projects.removeIf(project -> project.getUuid().equals(uuid));
    }

    public void deleteAll() {
        projects.clear();
    }
}
