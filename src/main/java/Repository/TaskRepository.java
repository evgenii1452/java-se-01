package Repository;

import Entity.Task;

import java.util.ArrayList;

public class TaskRepository {
    ArrayList<Task> tasks = new ArrayList<>();

    public void persist(Task task) {
        tasks.add(task);
    }

    public boolean update(String uuid, String taskName) {
        for (Task task: tasks) {
            if (task.getUuid().equals(uuid)) {
                task.setName(taskName);

                return true;
            }
        }

        return false;
    }

    public ArrayList<Task> getAll(String projectId) {
        ArrayList<Task> tasks = new ArrayList<>();

        for (Task task: this.tasks) {
            if (task.getProjectId().equals(projectId)) {
                tasks.add(task);
            }
        }

        return tasks;
    }

    public Task find(String uuid) {
        for (Task task: tasks) {
            if (task.getUuid().equals(uuid)) {
                return task;
            }
        }

        return null;
    }

    public void delete(String uuid) {
        tasks.removeIf(project -> project.getUuid().equals(uuid));
    }

    public void deleteAll(String projectId) {
        tasks.removeIf(task -> task.getProjectId().equals(projectId));
    }
}
