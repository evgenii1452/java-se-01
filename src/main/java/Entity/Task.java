package Entity;

public class Task {
    private String name;
    private final String uuid;
    private final String projectId;

    public Task(String projectId, String uuid, String name) {
        this.projectId = projectId;
        this.uuid = uuid;
        this.name = name;
    }

    public String getName() {
        if (name != null) {
            return name;
        }

        return "Без названия";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public String getProjectId() {
        return projectId;
    }
}
