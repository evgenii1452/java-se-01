package Entity;


public class Project {
    private String name;
    private final String uuid;

    public Project(String uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public String getName() {
        if (name != null) {
            return name;
        }

        return "Без названия";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }
}
